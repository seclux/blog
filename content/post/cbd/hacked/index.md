---
title: "CBD: Hacked" # Title of the blog post.
date: 2022-04-08T13:42:19+07:00 # Date of post creation.
description: "Cyberdefenders.org - Hacked Writeup" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "" # Sets featured image on blog post.
thumbnail: "images/cbd.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "images/cbd.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: true # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Technology
tags:
  - Linux
  - Forensics
  - Cyberdefenders.org
  - Blue Team
  - Writeup
  - Study 
# comment: false # Disable comment if false.
---

# **CyberDefenders: Hacked**
![hacked](img/hacked.png)

> You have been called to analyze a compromised Linux web server. Figure out how the threat actor gained access, what modifications were applied to the system, and what persistent techniques were utilized. (e.g. backdoors, users, sessions, etc).

# Tools:
-   [FTKImager](https://accessdata.com/products-services/forensic-toolkit-ftk/ftkimager)
-   [R-studio recovery](https://www.r-studio.com/)
-   [Guide: mounting challenge disk image on Linux](https://bwiggs.com/posts/2021-07-25-cyberdefenders-hacked/)

Download the challenge file by clicking the download challenge button. Unzip the file as follows:

```bash
unzip c53-Hacked.zip
```

![files](img/challengefiles.png)

What is the .E01 file format?:
> Developed by ASR Data, the Expert Witness file format (aka E01 format aka EnCase file format) is an industry standard format for storing “forensic” images. The format allows a user to access arbitrary offsets in the uncompressed data without requiring decompression of the entire data stream. The specification does NOT provide for quantifyable assurance of integrity, it is up to the implementation to provide meaningful authentication for any data contained in an “evidence file”.
\- [.EO1 format](http://www.asrdata.com/whitepaper-html)
- [Expert Witness Disk Image Format](https://www.loc.gov/preservation/digital/formats/fdd/fdd000406.shtml)

I work as root in my linux vm's. If you don't then add `sudo` before the commands.

To mount the .E01 image I followed the guide provided above in the tools section:

Install the required tools:
```bash
apt install ewf-tools sleuthkit kpartx
```

Create a mountpoint and mount the .E01 image:
```bash
mkdir image
```
```bash
ewfmount Webserver.E01 image
```

![ewfmount](img/ewfmount.png)

```bash
ls -la image
```

> mmls - Display the partition layout of a volume system (partition tables) 


```bash
mmls image/ewf1
```

![mmls](img/mmls.png)

Create a mountpoint for the partition: 

```bash
mkdir hacked.dsk
ls -la
```

> kpartx - Create device maps from partition tables

> lvscan - List all logical volumes in all volume groups


```bash
kpartx -a -v image/ewf1
lvscan
```

Mount the partition to the mountpoint in read-only mode: 
```bash
mount -o ro,noload /dev/VulnOSv2-vg/root hacked.dsk
ls -la hacked.dsk
```

We can check the mount as follows:
```bash
mount | grep hacked.dsk
```

![check mount](img/chkmount.png)

All set, now we can get to work:

# Questions
### 1. What is the system timezone?
```bash
cd hacked.dsk
cat etc/timezone
```

![q1](img/q1.png)

### 2. Who was the last user to log in to the system?
```bash
tail -20 var/log/auth.log
```
```bash
mail
```

### 3. What was the source port the user 'mail' connected from?
```bash
tail -20 var/log/auth.log | grep sshd | grep -i accepted
```
```bash
57708
```
### 4. How long was the last session for user 'mail'? (Minutes only)
```bash
1
```

![q2-3-4-5](img/q2-3-4-5.png)

### 5. Which server service did the last user use to log in to the system?
```bash
sshd
```

### 6. What type of authentication attack was performed against the target machine?

```bash
bruteforce
```
We can see loads of authentication failures in var/log/auth.log for username root indicating the attacker is trying a bruteforce attack

![bruteforce](img/q6.png)

### 7. How many IP addresses are listed in the '/var/log/lastlog' file?
```bash
cat var/log/lastlog
```
or
```bash
strings var/log/lastlog
```

![lastlog](img/q7.png)

```bash
2
```

### 8. How many users have a login shell?
```bash
cat etc/passwd
```
At first glance it looks like only /bin/bash is used
```bash
cat etc/passwd | grep /bin/bash
```
If you don't want to count yourself:
```bash
cat etc/passwd | grep /bin/bash | wc -l
```
```bash
5
```

### 9. What is the password of the mail user?
For this we need to extract the mail users' information out of etc/passwd and etc/shadow. Then use the unshadow tool and john the ripper to crack the hashfile:
```bash
cat hacked.dsk/etc/passwd > passwd
cat hacked.dsk/etc/shadow > shadow
unshadow passwd shadow > unshadowed
cat unshadowed | grep mail > mail.hash
```
The path of the wordlist will most likely be different, so change it accordingly:
```bash
john --wordlist=/usr/share/wordlists/rockyou.txt mail.hash
```

![crack password](img/q9.png)

```bash
forensics
```

### 10. Which user account was created by the attacker?
```bash
cd hacked.dsk
cat var/log/auth.log | grep useradd
```

![useradd](img/q10.png)

```bash
php
```

### 11. How many user groups exist on the machine?
```bash
cat etc/group | wc -l
```
```bash
58
```

### 12. How many users have sudo access?
First let's check etc/sudoers to see who and which groups can elevate privileges:
```bash
cat etc/sudoers
```
Members of the admin and sudo group can elevate privileges. There are no specific usernames mentioned in the sudoers file.
So let's check which users are in the admin and sudo groups:
```bash
grep --color "^admin:\|^sudo:" etc/group
```
Only the sudo group exists on this system and it has 2 users in it: php, mail
```bash
2
```

![sudoers](img/q12.png)

### 13. What is the home directory of the PHP user?
```bash
grep php etc/passwd
```

![home dir](img/q13.png)

or 
```bash
grep useradd var/log/auth.log | grep php
```

![home dir](img/q13a.png)
The -d option in useradd command specifies the home directory

```bash
/usr/php
```

### 14. What command did the attacker use to gain root privilege? (Answer contains two spaces).

```bash
grep -B 10 "Successful" var/log/auth.log
```

![command](img/q14.png)

```bash
sudo /bin/su -
```

### 15. Which file did the user 'root' delete?
```bash
grep rm root/.bash_history
```

```bash
37292.c
```

![delete](img/q15.png)

This however doesn't tell us where it was deleted from so we want to check some lines before this command too in .bash_history: 

```bash
grep -B 5 "rm" root/.bash_history
```

![delete](img/q15a.png)

The file was deleted from the /tmp/ directory.


### 16. Recover the deleted file, open it and extract the exploit author name.
> R-Studio for Linux
Demo Mode allows you to evaluate R-Studio for Linux for free. R-Studio Demo Mode is limited to files smaller than 256 KB in Demo Mode. You can, however, preview supported files that are larger than 256 KB by double-clicking it. This allows you to predict your chances for successful recovery before purchasing an R-Studio for Linux license.
\- [R-Studio](https://www.r-studio.com/data_recovery_linux/Download.shtml)

Download the appropriate version for your system and install it: 

```bash
wget https://www.r-studio.com/downloads/RStudio4_x64.deb
dpkg -i RStudio4_x64.deb
```

```bash
rstudio
```
Click on Demo

![rstudio1](img/rstudio1.png)

Locate the hacked.dsk Volume and click Show Files

![rstudio1](img/rstudio2.png)

Browse to the /tmp directory and select the file you want to restore. Then click Recover

![rstudio1](img/rstudio3.png)

We can't recover to hacked.dsk because it is mounted as read-only. So select one directory up and click Ok.
Close rstudio.

![rstudio1](img/rstudio4.png)

Check the recovered file to retrieve the Author: 

```bash
ls -la
cat 37292.c | less
```

![rstudio1](img/q16.png)


```bash
rebel
```

### 17. What is the content management system (CMS) installed on the machine?
First let's see what we should be looking for on google: 

![CMS System](img/q17a.png)

Configuration files are in the /etc/ directory on Linux so that is a good place to start looking:

![Drupal](img/q17b.png)

```bash
drupal
```


### 18. What is the version of the CMS installed on the machine?
Now we have an easy search in the log files: 
```bash
grep -i drupal var/log/apt/history.log
```

![drupal version](img/q18.png)

```bash
7.26
```

### 19. Which port was listening to receive the attacker's reverse shell?

From the recovered file we know the owner is www-data. This is the user used by webservers (apache / nginx, ...)
We also know Drupal 7 is running on the system. A quick search leads us to:

> ![drupal 7](img/q19a.png)
\- [drupal.org](https://www.drupal.org/docs/7/system-requirements/web-server)

Let's check if we have log files for webservers on our system: 

![logs](img/q19.png)

```bash
cat var/log/apache2/access.log
```

Some entries don't seem normal in this log file:

![apache2 log](img/q19b.png)

It looks like POST requests were made encoded in base64

![apache2 log](img/q19c.png)

After base64_decode we see: %28. This is a urlencoded (
If we search for the closing ) in urlencoded format: %29 we can locate the end of the base64 encoded string and then decode it: 

```bash
grep -i "base64" var/log/apache2/access.log | grep "%29"
```

![base64](img/q19d.png)

Decoding the base64 string: 

![base64 -d](img/q19e.png)

```bash
echo "base64string" | base64 -d
```

The listening port used for the reverse shell is: 

```bash
4444
```

# Conclusion

The threat actors gained access through a vulnerability in drupal 7 allowing remote code execution.

Searchsploit has the following Metasploit Modules for drupal: 

```bash
searchsploit drupal | grep -i metasploit
```

![searchsploit](img/c0.png)

The first one needs to be (authenticated), so let's take a look at `php/remote/44482.rb`

```bash
cat /usr/share/exploitdb/exploits/php/remote/44482.rb |less
```

![44482.rb](img/c1.png)

That looks very familiar from the apache2 access logs

![access logs](img/c2.png)

From here the attackers have a foothold as user www-data and escalated their privileges to root with the 37292.c exploit, which they deleted as seen in the .bash_history.

They added a new user `php` that has sudo access.
They changed the password and shell of user `mail`  and added him to the sudo group.

They created a php webshell in 
```bash
var/www/html/jabc/scripts/update.php
```

![webshell](img/c3.png)

and made sure it works correctly: 

![webshell](img/c4.png)

---

Really enjoyed this challenge. Solving the questions went pretty fast, but writing it all out and really figuring out what happened took some more time.

If I got something wrong, missed something or you know an easier way of doing things, don't hesitate to contact me. 


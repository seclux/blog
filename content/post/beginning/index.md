---
title: "Beginnings" # Title of the blog post.
date: 2021-07-10T14:18:33+02:00 # Date of post creation.
description: "Article description." # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "images/thebeginning.png" # Sets featured image on blog post.
thumbnail: "images/thebeginning.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "images/thebeginning.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Technology
tags:
  - Info
  - General
# comment: false # Disable comment if false.
---

It has been a long road to get here, but we are finally online.
Took me a lot of googling / reading / installing / deleting and learning markdown to what you are seeing now. Getting something online was the main purpose. A place where I can document my venture into infosec.

What to expect:

* Writeups
* Posts about my studies

As this will be a perpetual work in progress you can expect updates and tweaks to the site along the way.



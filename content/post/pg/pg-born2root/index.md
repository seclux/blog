---
title: "PG: Born2root" # Title of the blog post.
date: 2022-03-06T21:05:16+07:00 # Date of post creation.
description: "Offsec Proving Grounds: Born2Root Write Up" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "img/b2r.png" # Sets featured image on blog post.
thumbnail: "images/b2r.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "img/b2r.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: true # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Technology
tags:	
  - Proving Grounds
  - OffSec
  - Writeup
  - Study
  - Linux
# comment: false # Disable comment if false.
---

# *Proving Grounds - Born2Root*

I recently created an account on [Proving Grounds Play](https://www.offensive-security.com/labs/individual/). The free account gives you 3hours a day access to vulnerable machines. So I decided to make the most out of it and finally start practicing my report writing skills.
Without further ado ... here we go: 

# Scanning & Enumeration

We start with an initial scan to find open ports and services running on those ports.
I am using [rustscan](https://github.com/RustScan/Rustscan) because it is alot faster than nmap.

`─# rustscan -a 192.168.151.49 -- -sC -sV -oA nmap/initial`
- -sC: run default scripts
- -sV: detect the service version
- -oA: output in all formats and store in nmap/initial

![rustscan1](img/rustscan1.png)
![rustscan2](img/rustscan2.png)
We see that the following ports are open:
-	Port 22: running OpenSSH 6.7p1
-	Port 80: running Apache 2.4.10
-	Port 111: running rpcbind portmapper service

The scan also shows 2 disallowed entries in robots.txt:
-	/wordpress-blog
-	/files

Let's see what is hosted on the Webserver:
![secret_site](img/secretsec_site.png)
Main page of the Secretsec website gives us 3 names and an email address:
- Martin N
- Hadi M
- Jimmy S

The email address martin@secretsec.com reveals a naming convention. 
Possible usernames could be: Martin, Hadi and Jimmy.

Now let's take a look at the 2 directories that we found in robots.txt:
![/wordpress-blog](img/wordpress-blog.png)
![/files](img/files.png)
Unfortunately nothing useful.

With gobuster we can search for more directories
![gobuster](img/gobuster.png)

We find a directory called /icons: 

![/icons](img/icons.png)
One file stands out. Let's take a closer look. Downloading the file with wget to our local machine and viewing the contents it is a RSA Private key.
In order to use it we rename it and change the permissions on the file to 600.
`chmod 600 id_rsa`

# Gaining Access ("Exploitation")

![id_rsa](img/id_rsa.png)
Now we can try the usernames we previously found combined with the private key to get SSH access.
![ssh martin](img/ssh_martin.png)
At the password prompt we can just press enter and we are now connected as user martin.
![local.txt](img/local.png)
We find our first flag: `local.txt`

With `cat /etc/passwd` we can verify if our assumption regarding the usernames was correct:
![/etc/passwd](img/passwd.png)

# Privilege Escalation
Nothing interesting in the bash_history but `cat /etc/crontab` reveals a cronjob for user jimmy:
![crontab](img/crontab.png)
Since the file doesn't exist we can create our own python script to run here. Let's make a reverse shell.

![python](img/python-revshell.png)

Replace yourIP with the IP where the reverse shell should connect to and save it as `/tmp/sekurity.py`
We make the file executable as follows:
`chmod +x /tmp/sekurity.py`

And open a listener on our local machine using netcat: 
`nc -lvnp 4444`

![netcat](img/nc.png)

When sekurity.py gets executed we should get a reverse shell as user jimmy.
![jimmy](img/jimmyshell.png)
![networker](img/networker.png)
We find this file called networker. I wasn't able to figure out how to use it in order to privesc to root user or if it was even the intended way.

Instead I bruteforced the ssh account for hadi with hydra.
We can create a custom passwordlist for hadi as follows:

`grep hadi /usr/share/wordlists/rockyou.txt > born2roothadi.txt`

![wordlist](img/wordlist.png)
![hydra](img/hydra.png)
![hadi ssh](img/ssh_hadi.png)
As user hadi we can switch user to root with hadi's password: 
![proof](img/proof.png)

# Recommended Mitigations
-	Don't save private keys in files facing the internet.
-	Make sure there are no cronjobs running from the /tmp directory. Anyone has access to this directory.
-	Have a decent password policy in place so that `username123` and other easy to guess passwords are not possible

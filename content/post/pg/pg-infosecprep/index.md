---
title: "PG: Infosecprep" # Title of the blog post.
date: 2022-03-20T17:28:24+07:00 # Date of post creation.
description: "OffSec Proving Grounds: InfoSecPrep Write Up" # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "" # Sets featured image on blog post.
thumbnail: "images/pg.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "images/seclux.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Technology
tags:
  - Proving Grounds
  - OffSec
  - Writeup
  - Study
  - Linux
# comment: false # Disable comment if false.
---

# *Proving Grounds - InfosecPrep*

# Scanning & Enumeration

As always we start by scanning for open ports: 
![rustscan](img/rustscan.png)
Two ports come back as open:
- Port 22: running OpenSSH 8.2p1
- Port 80: running Apache 2.4.41

![](img/rustscan1.png)
We also see a `secret.txt` file in the disallowed list from `robots.txt`
![](img/site.png)
Reading the website we discover that the only user on this machine is called: oscp
The secret.txt looks like a base64 encoded file:

![](img/secret.txt.png)

Decoding reveals it is a SSH Private key
![](img/b64.png)

# Gaining Access ("Exploitation")

Let's save it as `id_rsa` and set the permissions to 600. We can now try to connect as oscp over SSH:
![](img/local.png)

Now that we have a foothold we can start with Privesc.
Start a simple httpserver on your local machine to host linpeas.sh and transfer it:
Local machine:
`python3 -m http.server 80`
![](img/httpserver.png)
![](img/httptransfer.png)

# Privilege Escalation

`chmod +x linpeas.sh` and run it.
![](img/linpeas.png)

The interesting Attack Vectors will be marked in Red/Yellow
![](img/linpeas1.png)

CVE-2021-4034:
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-4034
I will try this one when I find the time, but it looks vulnerable. However I am not sure this CVE existed when the infosecprep was released.
![](img/cve-2021-4034.png)

Next we find this in the linpeas output

![](img/linpeas2.png)

/bin/bash with sticky bits seems really interesting and easy to exploit. 
`/bin/bash -p`  will run /bin/bash and preserve the permissions set on it. Meaning we can spawn a bash shell with root privileges

![](img/privesc.png)

# Recommended Mitigations
- Never expose passwords and private keys even if they are encoded
- Be warry about using Suid/Guid bits
---
title: "TryHackMe: Pre Security Learning Path" # Title of the blog post.
date: 2021-07-12T15:51:02+02:00 # Date of post creation.
description: "TryHackme Pre Security Review" # Description used for search engine.
featured: false # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
featureImage: "images/1-LearningPath.png" # Sets featured image on blog post.
thumbnail: "images/1-LearningPath1.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "images/1-LearningPath.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Technology
tags:
  - TryHackMe
  - Review
# comment: false # Disable comment if false.
---

If you ever had the feeling that there is too much information out there and you have no idea where to start your journey into infosec then you can stop worrying now because [TryHackMe](https://tryhackme.com) has you covered.

The [Pre Security Learning Path](https://tryhackme.com/path/outline/presecurity) is just the right place for you!

Let's see what it is all about:   

# 1. Cyber Security Introduction
![Cyber Security Introduction](images/2-cybersecintro.png)

# 2. Network Fundamentals
![Network Fundamentals](images/3-networkfunda.png)

# 3. How the Web Works
![How the Web Works](images/4-howthewebworks.png)

# 4. Linux Fundamentals
![Linux Fundamentals](images/5-linuxfunda.png)

# 5. Windows Fundamentals
![Windows Fundamentals](images/6-winfunda.png)

Tryhackme has done an amazing job breaking all these topics down into small tasks. Each task has well written and easy to understand explanations about the needed knowledge. Together with the pictures and interactive content they make learning complex topics fun and seem easy. They even put in a game to learn the OSI Model.

![OSI Model Game](images/7-osigame.png)

After completing the Learning Path you will have earned a couple of badges (badges are cool) and you get a Certificate of Completion.

![Certificate of Completion](images/8-cert.png)


I can recommend the [Pre Security Learning Path](https://tryhackme.com/path/outline/presecurity) to anyone curious about security and for people with already some experience it can be a nice refresher on a variety of topics.

Great job [TryHackMe](https://tryhackme.com) !!

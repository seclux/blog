+++
title = "whoami.txt"
description = ""
date = "2021-07-01"
aliases = ["about-us", "contact"]
author = "Kristof De Smedt"
+++

# Kristof De Smedt aka deLuxy

Still under construction, but I can already share a picture of where it all started:

![Sinclair ZX Spectrum](images/zx_spectrum.png)



![THM-Badge](https://tryhackme-badges.s3.amazonaws.com/deLuxy.png)
